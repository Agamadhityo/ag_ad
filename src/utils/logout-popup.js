import { removeClass, addClass } from './utils'

export default function () {
    const logoutButtonElement = document.querySelector('.profile__logout-button');

    const modalElement = document.querySelector('#modal-backdrop')

    const modalButtonElement = document.querySelectorAll('#modal-backdrop a')

    logoutButtonElement.addEventListener('click', function (event) {
        addClass(modalElement, 'active')
    });

    modalElement.addEventListener('click', function (ev) {
        removeClass(ev.target, 'active')
    });

    for (let i = 0; i < modalButtonElement.length; i++) {
        const element = modalButtonElement[i];
        element.addEventListener('click', function (ev) {
            alert(this.text + ' button clicked');
        });
    }
}