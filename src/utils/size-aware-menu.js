import { removeClass, addClass, hasClass } from './utils'

export default function () {

    const menuButton = document.querySelectorAll('.profile__list-tab-button');


    function windowResize() {
        const button = document.querySelector('.profile__list-tab-button-6').getBoundingClientRect();
        const sizeAwareMenuButton = document.querySelector('.profile__list-window-aware');

        if (button.right > (window.innerWidth - 165)) {
            if (!hasClass(sizeAwareMenuButton, 'visible')) {
                addClass(sizeAwareMenuButton, 'visible')
            }
        } else {
            removeClass(sizeAwareMenuButton, 'visible')
        }

        console.log(menuButton);

        for (let i = 0; i < menuButton.length; i++) {
            console.log(element)
            const element = menuButton[i].getBoundingClientRect();

            if (element.right > (window.innerWidth - 165)) {
                if (!hasClass(element, 'visible')) {
                    addClass(element, 'visible')
                }
            } else {
                removeClass(element, 'visible')
            }
        }

    };

    window.onresize = windowResize;
}