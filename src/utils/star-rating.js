export default function() {
    const starElements = document.querySelectorAll('.star-rating');

    for (let i = 0; i < starElements.length; i++) {
        const component = starElements[i];
        const star = component.dataset.star;
        let starLeft = 5 - star;
        let stars = "";

        for (let j = 0; j < star; j++) {
            stars += '<ion-icon name="star"></ion-icon>';
        }

        for (let k = 0; k < starLeft; k++) {
            stars += '<ion-icon name="star-outline"></ion-icon>';
        }

        component.innerHTML = stars;
    }
}