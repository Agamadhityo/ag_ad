import { removeClass, addClass, hasClass } from './utils'

export default function() {
    const editButtonElements = document.querySelectorAll('.profile__body-edit-button');
    const actionButtonElements = document.querySelectorAll('.profile__body-action-button');

    // Attach event listener to edit button
    for (let i = 0; i < editButtonElements.length; i++) {
        editButtonElements[i].addEventListener('click', function() {
            const parentGroupElement = this.parentNode.parentNode.parentNode;
            const fieldVal = this.parentNode.querySelector('.profile__body-value').innerText;
            removeClass(parentGroupElement.parentNode.querySelectorAll('.profile__body-form-field'), 'active');
            addClass(parentGroupElement.querySelector('.profile__body-form-field'), 'active');
            addClass(parentGroupElement.querySelector('.profile__body-form-field input'), 'dirty');
            parentGroupElement.querySelector('.profile__body-form-field input').value = fieldVal
        });
    }

    // Attach event listener to save button desktop
    for (let i = 0; i < actionButtonElements.length; i++) {
        if (hasClass(actionButtonElements[i], 'save-button')) {
            actionButtonElements[i].addEventListener('click', function() {
                const parentFieldElement = this.parentNode.parentNode;
                const parentGroupElement = parentFieldElement.parentNode;
                const fieldVal = parentGroupElement.querySelector('.profile__body-form-field input').value;

                if (!fieldVal) {
                    return;
                }

                if (hasClass(parentFieldElement, 'name')) {
                    document.querySelector('.profile__bio-name').innerText = fieldVal;
                }

                if (hasClass(parentFieldElement, 'phone')) {
                    document.querySelector('.profile__bio-phone span').innerText = fieldVal;
                }

                if (hasClass(parentFieldElement, 'address')) {
                    document.querySelector('.profile__bio-location span').innerText = fieldVal;
                }

                parentGroupElement.querySelector('.profile__body-value').innerText = fieldVal;
                removeClass(parentFieldElement, 'active');
            });
        } else {
            actionButtonElements[i].addEventListener('click', function() {
                const parentFieldElement = this.parentNode.parentNode;
                removeClass(parentFieldElement, 'active');
            });
        }
    }

    // Initiate edit mobile
    document.querySelector('.profile__mobile-action-edit').addEventListener('click', function() {
        const profileContentElement = this.parentNode.parentNode;
        addClass(profileContentElement, 'edited');

        const formGroupsElements = profileContentElement.querySelectorAll('.profile__body-form-group');

        for (let i = 0; i < formGroupsElements.length; i++) {
            const item = formGroupsElements[i];
            const itemText = item.querySelector('.profile__body-value').innerText;
            item.querySelector('.profile__body-form-field input').value = itemText;
            addClass(item.querySelector('.profile__body-form-field input'), 'dirty');
        }
    });

    // Cancel mobile
    document.querySelector('.profile__mobile-action-button.cancel-button').addEventListener('click', function() {
        const profileContentElement = this.parentNode.parentNode;
        removeClass(profileContentElement, 'edited');
    });

    // Save mobile
    document.querySelector('.profile__mobile-action-button.save-button').addEventListener('click', function() {
        const profileContentElement = this.parentNode.parentNode;
        removeClass(profileContentElement, 'edited');

        const formGroupsElements = profileContentElement.querySelectorAll('.profile__body-form-group');

        for (let i = 0; i < formGroupsElements.length; i++) {
            const item = formGroupsElements[i];
            const fieldVal = item.querySelector('.profile__body-form-field input').value;
            
            if (!fieldVal) {
                continue;
            }

            if (hasClass(item.querySelector('.profile__body-form-field'), 'name')) {
                document.querySelector('.profile__bio-name').innerText = fieldVal;
            }

            if (hasClass(item.querySelector('.profile__body-form-field'), 'phone')) {
                document.querySelector('.profile__bio-phone span').innerText = fieldVal;
            }

            if (hasClass(item.querySelector('.profile__body-form-field'), 'address')) {
                document.querySelector('.profile__bio-location span').innerText = fieldVal;
            }

            item.querySelector('.profile__body-value').innerText = fieldVal;
        }
    });
}