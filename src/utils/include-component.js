import { removeClass, addClass, hasClass } from './utils'

export default function () {
  const importedComponents = document.querySelectorAll('.include-component');

  for (let i = 0; i < importedComponents.length; i++) {
    const component = importedComponents[i];
    const toInclude = component.dataset.include;
    fetch(toInclude)
      .then(function (response) {
        return response.text();
      })
      .then(function (html) {
        component.innerHTML = html;
      })
      .then(function () {
        const profileElement = document.querySelector('#profile');
        const profileTabButton = profileElement.querySelectorAll('.profile__list-tab-button');

        for (let i = 0; i < profileTabButton.length; i++) {
          profileTabButton[i].addEventListener('click', function (event) {
            event.preventDefault();
            removeClass(document.querySelectorAll('.profile__list-tab-button'), 'active');
            removeClass(document.querySelectorAll('.profile__content'), 'edited');
            removeClass(document.querySelectorAll('.profile__content'), 'active');
            addClass(this, 'active')
            addClass(document.querySelector(profileTabButton[i].querySelector('a').getAttribute('href')), 'active');
          }, false);
        }

        //==============

        document.querySelector('.profile__list-window-aware').addEventListener('click', function () {
          if (!hasClass(this.querySelector('ul'), 'visible')) {
            addClass(this.querySelector('ul'), 'visible')
          } else {
            removeClass(this.querySelector('ul'), 'visible')
          }
        });

        window.onresize = () => {
          const button6 = document.querySelector('.profile__list-tab-button-6');
          const button5 = document.querySelector('.profile__list-tab-button-5');
          const sizeAwareMenuButton = document.querySelector('.profile__list-window-aware');
          const buttonAware6 = sizeAwareMenuButton.querySelector('.profile__list-tab-button-6');
          const buttonAware5 = sizeAwareMenuButton.querySelector('.profile__list-tab-button-5');

          if (button6.getBoundingClientRect().right > (window.innerWidth - 165)) {
            if (!hasClass(sizeAwareMenuButton, 'visible')) {
              addClass(button6, 'hidden')
              addClass(buttonAware6, 'visible')
              addClass(sizeAwareMenuButton, 'visible')
            }
          } else {
            removeClass(button6, 'hidden')
            removeClass(buttonAware6, 'visible')
            removeClass(sizeAwareMenuButton, 'visible')
          }

          if (button5.getBoundingClientRect().right > (window.innerWidth - 180)) {
            addClass(button5, 'hidden')
            addClass(buttonAware5, 'visible')
          } else {
            removeClass(button5, 'hidden')
            removeClass(buttonAware5, 'visible')
          }
        };
      });
  }
}