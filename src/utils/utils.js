function removeClass(elements, removedClass) {
    if (!elements.length) {
        elements.className = elements.className.replace(new RegExp('(?:^|\\s)' + removedClass + '(?:\\s|$)'), '');
        return;
    }

    for (let i = 0; i < elements.length; i++) {
        elements[i].className = elements[i].className.replace(new RegExp('(?:^|\\s)' + removedClass + '(?:\\s|$)'), '');
    }
}

function addClass(elements, addedClass) {
    if (!elements.length) {
        elements.className += ' ' + addedClass;
        return;
    }

    for (let i = 0; i < elements.length; i++) {
        elements[i].className += ' ' + addedClass;
    }
}

function hasClass(element, checkedClass) {
    var i;
    var classes = element.className.split(" ");
    for (i = 0; i < classes.length; i++) {
        if (classes[i] == checkedClass) {
            return true;
        }
    }
    return false;
}

function dirtyField() {
    const elements = document.querySelectorAll('input');

    for (let i = 0; i < elements.length; i++) {
        elements[i].addEventListener('keyup', function () {
            if (this.value.length == 0) {
                removeClass(this, 'dirty');
            } else {
                removeClass(this, 'dirty');
                addClass(this, 'dirty')
            }
        });
    }
}

export { removeClass, addClass, hasClass, dirtyField }