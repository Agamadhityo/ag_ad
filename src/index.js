import include from "./utils/include-component";
import star from "./utils/star-rating";
import editableField from "./utils/editable-field";
import logoutPopup from "./utils/logout-popup";
import sizeAwareMenu from "./utils/size-aware-menu";
import { dirtyField } from "./utils/utils"

star();
include();
dirtyField();
editableField();
logoutPopup();
sizeAwareMenu();